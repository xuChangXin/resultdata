package com.chang.resultdatademo.controller;

import com.chang.resultdatademo.common.ResultData;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResultDataController {

    @GetMapping("/hello")
    public ResultData<String> hello(){
        return ResultData.success("hello");
    }

    @GetMapping("/hello2")
    public String hello2(){
        return "hello2";
    }

    @GetMapping("/hello3")
    public void hello3(){
        int i = 1/0;
    }

    @GetMapping("/hello4")
    public void hello4(){
        int[] ints = new int[1];
        int anInt = ints[10];
    }
}
