package com.chang.resultdatademo.common;

import lombok.Data;

@Data
public class ResultData<T> {

    private Integer status;
    private String message;
    private T data;
    private Long timeStamp;
    private Integer httpStatus;

    public ResultData(){
        timeStamp = System.currentTimeMillis();
    }


    public static <T> ResultData<T> success(T data){
        ResultData<T> resultData = new ResultData<>();
        resultData.setStatus(ResultCode.RC100.getCode());
        resultData.setMessage(ResultCode.RC100.getMessage());
        resultData.setData(data);
        return resultData;
    }

    public static <T> ResultData<T> fail(int code,String message){
        ResultData<T> resultData = new ResultData<>();
        resultData.setStatus(code);
        resultData.setMessage(message);
        return resultData;
    }
}


