package com.chang.resultdatademo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResultDataDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ResultDataDemoApplication.class, args);
    }

}
